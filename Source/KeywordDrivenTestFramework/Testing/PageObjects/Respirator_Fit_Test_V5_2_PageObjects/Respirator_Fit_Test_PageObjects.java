/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Respirator_Fit_Test_V5_2_PageObjects;

/**
 *
 * @author skhumalo
 */
public class Respirator_Fit_Test_PageObjects
{
    public static String PermitToWork_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_41B8AE76-F4F5-4CA6-97C2-8DCB7A749951']";
    }    
    
    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }
    
    public static String OccupationalHygene_Module()
    {
        return "//label[text()='Occupational Hygiene']";
    }

    public static String RespiratorFitTest_Module()
    {
        return "//label[text()='Respirator Fit Test']";
    }

    public static String RespiratorFitTest_Add()
    {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }

    public static String BusinessUnitTab()
    {
        return "//div[@id='control_35B52F0B-F0A2-4698-8694-96F4F53F3038']";
    }

    public static String BusinessUnit_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }

    public static String TailingsFacilityTab()
    {
        return "//div[@id='control_83D59440-6671-44B7-BE65-98C127F9F16B']";
    }

    public static String MonthTab()
    {
        return "//div[@id='control_7B0C9473-3540-44B8-AA54-C6D86F1F1491']";
    }

    public static String YearTab()
    {
        return "//div[@id='control_88D63725-BADF-41F3-800A-A1D3D24767D3']";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@class='linkbox-link']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String SaveSupDocs()
    {
        return "//div[@id='control_17FC5C89-114C-4159-B637-8961042DAA55']";
    }

    public static String ProductionRegister_Save()
    {
        return "//div[@id='btnSave_form_F59507A9-C4B4-469A-A955-73864FF1F960']";
    }

    public static String ProductionRegister_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_F59507A9-C4B4-469A-A955-73864FF1F960']";
    }
    
    public static String ProductionRegisterFindings_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_8FC01CD7-C90A-4A6E-8531-ED64B8C7D06E']";
    }
    
    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    
    public static String validateSave()
    {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String Measurements_Tab()
    {
        return "//div[text()='Measurements']";
    }

    public static String Measure_Field()
    {
        return "//div[@id='control_C89D32AC-67C6-4560-9CEE-452D790ADD38']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String Findings_Tab()
    {
        return "//div[text()='Findings']";
    }

    public static String ProductionRegisterFindings_Add()
    {
        return "//div[@id='control_9CC4FAE6-D714-4861-A4F3-55337BBBF53B']//div[text()='Add']";
    }

    public static String FindingDescription_Field()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String ProductionRegisterFindings_Save()
    {
        return "//div[@id='btnSave_form_8FC01CD7-C90A-4A6E-8531-ED64B8C7D06E']";
    }

    public static String ProductionRegisterFindings_SaveToContinue()
    {
        return "//div[@id='control_E0B5417B-8F24-4E1D-A4DB-8C68192F6F7B']";
    }

    public static String FindingOwnerTab()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }

    public static String FindingSourceTab()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }

    public static String FindingSource_Option(String data)
    {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String FindingClassificationTab()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }

    public static String close_ProductionRegisterFindings()
    {
        return "(//div[@id='formWrapper_8FC01CD7-C90A-4A6E-8531-ED64B8C7D06E']/..//i[@class='close icon cross'])[1]";
    }

    public static String RespiratorFitTest_ProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_3ED06EE9-E78A-4EF9-B791-FD0210A5E31C']";
    }

    public static String TestDate()
    {
        return "//div[@id='control_88F49A64-12DB-4450-8C8C-995AEB90EF19']//input[@type='text']";
    }

    public static String Name()
    {
        return "//div[@id='control_EDBB3841-13A9-47D8-AD7C-C5C2C4C021F9']";
    }

    public static String RespiratorType()
    {
        return "//div[@id='control_55A1EDDA-3EB1-46A7-9A0A-0F5701BE480E']";
    }

    public static String BrandModel()
    {
        return "//div[@id='control_B5B7565A-0756-407F-BC0E-031E5AD51EBC']//input[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String TestResultComments()
    {
        return "//div[@id='control_0DE9116C-6668-4262-B189-2A3556F915DB']//textarea[@language='A50A7F35-56F8-451E-82D9-946BD9ADEDB4']";
    }

    public static String RespiratorFitTest_Save()
    {
        return "//div[@id='btnSave_form_3ED06EE9-E78A-4EF9-B791-FD0210A5E31C']";
    }

    public static String TestResult()
    {
        return "//div[@id='control_F7DB012A-A944-4176-B712-CE1F02BBE07E']";
    }

    public static String SEG()
    {
        return "//div[@id='control_D9764AB4-179A-405D-A77F-A8B073D5E14B']";
    }
}
